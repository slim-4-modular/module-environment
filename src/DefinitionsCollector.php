<?php

declare(strict_types=1);

namespace Paneric\ModuleEnvironment;

class DefinitionsCollector
{
    private array $envConfigs = ['dev.php', 'test.php', 'prod.php'];
    private array $translations;

    public function prepareDefinitions(
        array $processFoldersPaths,
        string $definitionsFolderName,
        string $local = null,
        string $env = null
    ): array {
        $definitionsFoldersPaths = [];

        foreach ($processFoldersPaths as $path) {
            $definitionsFoldersPaths[] = $path . $definitionsFolderName . '/';
        }

        $definitions = [];
        $this->translations = [];
        foreach ($definitionsFoldersPaths as $scopePath) {
            $filesNames = glob($scopePath . '*/*.php');

            if (!is_dir($scopePath)) {
                continue;
            }

            $scopeDefinitionsFolderContent = array_diff(scandir($scopePath), ['.', '..']);

            foreach ($filesNames as $fileName) {
                if(is_file($fileName)) {
                    $fileName = $this->checkExtension($fileName);
                    $fileName = $this->checkTranslationSettings($fileName, $local);
                    $fileName = $this->checkEnvironmentSettings($fileName, $env);

                    if ($fileName !== null) {
                        $definitions[] = (array) require($fileName);
                    }
                }
            }
        }

        $definitions[]['translations'] = $this->translations;

        return array_merge([], ...$definitions);
    }

    private function checkExtension(string $fileName): ?string
    {
        if (pathinfo($fileName)['extension'] === 'php') {
            return $fileName;
        }

        return null;
    }

    private function checkTranslationSettings(string $fileName = null, string $local = null): ?string
    {
        if ($local === null || $fileName === null) {
            return $fileName;
        }

        if (str_contains($fileName, 'translation_' . $local . '.php')) {
            $this->translations = array_merge($this->translations, (array) require($fileName));
            return null;
        }

        return $fileName;
    }

    private function checkEnvironmentSettings(string $fileName = null, string $env = null): ?string
    {
        if ($env === null  || $fileName === null) {
            return $fileName;
        }

        $baseName = (pathinfo($fileName))['basename'];
        if (in_array($baseName, $this->envConfigs, true)) {
            if ($baseName === $env . '.php') {
                return $fileName;
            }

            return null;
        }

        return $fileName;
    }
}
