<?php

declare(strict_types=1);

return [
    'definitions_folder_name' => 'config',
    'module_resolver' => [
        'default_route_key' => 'main',
        'local_map' => ['en', 'pl'],
        'dash_as_slash' => false,
        'merge_module_cross_map' => false,
        'app_path'  => './../src/',// or './../Auth/' in case of Auth microservice
        'root_paths'  => ['./../'],// or package, for example './../vendor/paneric/Package/'
        'proxy_prefix' => '/auth-api',
    ],
    'default_local' => 'en',
    'local' => [
        'dev' => [
            'name' => 'scm-lng',
            'expire' => time() + 60 * 60 * 24 * 120, // 120 days
            'path' => '/',
            'domain' => $_ENV['APP_DOMAIN'],
            'security' => false, //true if it can be sent only by https
            'http_only' => true, //not accessible for javascript
        ],
    ],
];
