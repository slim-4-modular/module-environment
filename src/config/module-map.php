<?php

declare(strict_types=1);

return [
    '/auth-api' => [
        'action' => './../AuthApi/Action',
        'actions' => './../AuthApi/Action',
        'privilege' => './../AuthApi/Privilege',
        'privileges' => './../AuthApi/Privilege',
        'role' => './../AuthApi/Role',
        'roles' => './../AuthApi/Role',
        'credential' => './../AuthApi/Credential',
        'credentials' => './../AuthApi/Credential',

        'action-privilege' => './../AuthApi/ActionPrivilege',
        'action-privileges' => './../AuthApi/ActionPrivilege',
        'privilege-role' => './../AuthApi/PrivilegeRole',
        'privilege-roles' => './../AuthApi/PrivilegeRole',
        'role-credential' => './../AuthApi/RoleCredential',
        'role-credentials' => './../AuthApi/RoleCredential',

        'action-credential' => './../AuthApi/AcnCrd',
        'action-credentials' => './../AuthApi/AcnCrd',

        'jwt' => './../AuthApi/Jwt',
    ],
];
