<?php

declare(strict_types=1);

namespace Paneric\ModuleEnvironment;

class Local
{
    private ?string $value;

    public function setValue(array $config, string $defaultLocal, ?string $local = null): ?string
    {
        if ($local !== null) {
            setcookie($config['name'], $local, $config['options']);

            $this->value = $local;

            return $this->value;
        }

        if (!isset($_COOKIE[$config['name']])) {
            setcookie($config['name'], $defaultLocal, $config['options']);

            $this->value = $defaultLocal;

            return $this->value;
        }

        $this->value = $_COOKIE[$config['name']];

        return $this->value;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }
}
