<?php

declare(strict_types=1);

namespace Paneric\ModuleEnvironment;

class ModuleMapper
{
    protected CONST CONFIG_SUB_PATH = '/src/config/module-resolver-config.php';
    protected CONST MAP_CROSS_KEY = 'module_map_cross';

    public function setModuleMapCross(
        array $moduleMapCrossPaths,
        array $moduleMap,
        String $moduleMapPath,
        bool $merged
    ): array {
        $moduleMapCross = [];

        foreach ($moduleMapCrossPaths as $partialPath) {
            $subServiceConfig = require(ROOT_FOLDER . $partialPath . self::CONFIG_SUB_PATH);
            $subServiceModuleMapCross = $subServiceConfig[self::MAP_CROSS_KEY];

            foreach ($subServiceModuleMapCross as $routePart => $modulePath) {
                $moduleMapCross[$routePart] = $modulePath;
            }
        }

        if ($merged) {
            return array_merge(
                $this->setModuleMap($moduleMap, $moduleMapPath),
                $moduleMapCross
            );
        }

        return $moduleMapCross;
    }

    public function setModuleMap(array $moduleMap, String $relativePath = ''): array
    {
        foreach ($moduleMap as $index => $path) {
            if ($relativePath === '') {
                $path = str_replace('%s/', '', $path);
            }

            if ($relativePath !== '') {
                $path = sprintf($path, $relativePath);
            }

            $moduleMap[$index] = $path;
        }

        return $moduleMap;
    }
}
