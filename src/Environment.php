<?php

declare(strict_types=1);

namespace Paneric\ModuleEnvironment;

use Exception;

class Environment
{
    private string $proxyPrefix;
    private ?string $module = null;
    private array $processFoldersPaths;
    private string $local;
    private array $definitions;

    /**
     * @throws Exception
     */
    public function set(
        array $moduleEnvironmentConfig,
        array $moduleMap,
        string $env,
        bool $isApi = true
    ): self {
        $moduleResolver = new ModuleResolver();
        $moduleResolver->processRequest(
            $moduleEnvironmentConfig['module_resolver'],
            $moduleMap
        );
        $this->proxyPrefix = $moduleResolver->getProxyPrefix();
        $this->module = $moduleResolver->getModule();
        $this->processFoldersPaths = $moduleResolver->getProcessFoldersPaths();
        $this->local = $moduleResolver->getLocal() ?? $moduleEnvironmentConfig['default_local'];

        if (!$isApi) {
            $local = new Local();
            $this->local = $local->setValue(
                $moduleEnvironmentConfig['local'][$env],
                $moduleEnvironmentConfig['default_local'],
                $this->local
            );
        }

        $definitionsCollector = new DefinitionsCollector();
        $this->definitions = $definitionsCollector->prepareDefinitions(
            $this->processFoldersPaths,
            $moduleEnvironmentConfig['definitions_folder_name'],
            $moduleEnvironmentConfig['default_local'],
            $env
        );

        return $this;
    }

    public function getProxyPrefix(): string
    {
        return $this->proxyPrefix;
    }
    public function getModule(): ?string
    {
        return $this->module;
    }
    public function getProcessFoldersPaths(): array
    {
        return $this->processFoldersPaths;
    }
    public function getLocal(): string
    {
        return $this->local;
    }
    public function getDefinitions(): array
    {
        return $this->definitions;
    }
}
