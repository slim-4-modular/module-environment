# Module Environment

## 1. Introduction

This package has been created as a pursuit to fulfil `LOWN` (load only what needed) principle, related to resources.   

Currently, it consists of five classes:
- Paneric\ModuleEnvironment\ [ModuleResolver](#module-resolver)
- Paneric\ModuleEnvironment\ [Local](#local)
- Paneric\ModuleEnvironment\ [DefinitionsCollector](#definitions-collector)
- Paneric\ModuleEnvironment\ [LocalMiddleware](#local-middleware)
- Paneric\ModuleEnvironment\ [Environment](#environment)

## 2. Installation

```shell
$ composer require paneric/module-environment
```

## 3. Integration

### 3.1 Project structure

`my-project`
  - **src**
    - **bootstrap** `(1)`
      - bootstrap.php
      - middleware.php `(1)`
      - routes.php `(1)`
    - **config** `(1)`
      - **dependencies** `(1)` 
      - **settings** `(1)`
      - **translations** `(1)`
    - **Action(Service)** `(1)`
    - **Controller(ActionController)** `(1)`
    - **Infrastructure** `(1)`
    - **Model** `(1)`
    - **...** `(1)`
  - **Module**
    - **bootstrap** `(2)`
      - middleware.php `(2)` *(middleware instantiation call chain)*
      - routes.php `(2)` *(routes declaration)*
    - **config** `(2)`
      - **dependencies** `(2)` *(DI classes definitions)*
        - cam.php *(controller, action, model)*
        - middleware.php
        - packages.php
        - ...
      - **settings** `(2)` *(DI configurations declarations)* 
        - dev.php
        - prod.php
        - test.php
        - ...
      - **translations** `(2)` *(DI translations declarations)*
        - translation_en.php
        - ...
      - non-di-settings.php
      - ...
    - **Action** `(2)`
    - **Controller** `(2)`
    - **Infrastructure** `(2)`
    - **Model** `(2)`
    - **...** `(2)`

`(1)` files application related   
`(2)` files module related   
`( )` similarly for submodules related files

### 3.2 Configuration
1. .env
```.dotenv
#dev, prod, test

ENV=dev
...
```
2. see: [module-map.php](#module-map)
3. see: [module-resolver-config.php](#module-resolver-config)
4. see: [local-config.php](#local-config)

## 4. Module Resolver

1. Extracts `local` value:
    - from request url, under condition it is located as its last element: **http://domain/module/action** `/en` or
    - from request query parameters under condition it is declared as: **http://domain/module/action** `?local=en`.
2. Prepare an array of folders, potential resources locations `processFoldersPaths` related to a particular url request:
```php
<?php

$processFoldersPaths = [
    './../src',
    './../Module',
    './../Module/Submodule',
    ...
];
```
3. Extracts verified `proxy_prefix` value from configuration for a further use within routing:

> https:// domain.com / **proxy-prefix** / module-ref / action

For example, in case of microservice application architecture:

> https:// paneric.com / **auth** / roles / get *(frontend)*   
> https:// paneric.com / **auth-api** / roles / get *(backend)*

### 4.1 Possible modules locations, namespaces and routes

#### 4.1.1 Base module

>    - my-project
>        - src
>        - **Module**
>        - ... 

1. path:`./../Module;`    
2. namespace: `My\Module`;   
3. routes: 
    - `{/proxy-prefix}/module/some-action`;
    - `/module/some-action`;


#### 4.1.2 Submodule

>    - my-project
>        - src
>        - Module
>            - **Submodule**
>            - ...
>        - ... 

1. path: `./../Module/Submodule`;
2. namespace: `My\Module\Submodule`; 
3. routes:
    - `{/proxy-prefix}/module/submodule/some-action`;
    - `/module/submodule/some-action`;
    - `{/proxy-prefix}/submodule/some-action`;
    - `/submodule/some-action`;

### 4.2 Configuration

#### 4.2.1 <a name="module-map">module-map.php</a>

- `./../src/config/module-map.php` or
- `./../Module/config/module-map.php` or
- `./../Module/Submodule/config/module-map.php`

```php
<?php

declare(strict_types=1);

return [
    'main' =>  './../src' // or './../Core' or ... related to '/core/main' or '/main' url path
    '/auth-api' => [// Nginx proxy location (look at module-resolver-config.php)
        // Project modules
        'module' => './../Module',
        'submodule' => './../Module/Submodule',
        ...
        // Third party modules
        'v-module' => './../vendor/ThirdParty/Package/Module',
        'v-submodule' => './../vendor/ThirdParty/Package/Module/Submodule',
        ...
    ],
];
```

- `main` - default route `Module` part value. Prepared to serve invalid or unknown requests.  
- `/auth-api` - key related to scope of modules related to a `proxy_prefix`. 

#### 4.2.2 <a name="module-resolver-config">module-resolver-config.php</a>

- `./../src/config/module-resolver-config.php` or
- `./../Module/config/module-resolver-config.php` or
- `./../Module/Submodule/config/module-resolver-config.php`

```php
<?php

declare(strict_types=1);

return [
    'default_route_key' => 'main',
    'local_map' => ['en', 'pl'],
    'dash_as_slash' => false,
    'merge_module_cross_map' => false,
    'app_path'  => './../src/',// or './../Auth/' in case of a microservice
    'root_paths'  => [
        './../',
        './../vendor/ThirdParty/Package/'
    ],
    'proxy_prefix' => '/auth-api',// or '' alternatively
];
```

- `default_route_key` - indicates the key chosen as default in `module-map.php` used to redirect unknown urls.
- `local_map` - languages served by our application.
- `dash_as_slash` - in case if you prefer `/module-action` over `/module/action`.
- `merge_module_cross_map` - experimental (work in progress), related to `Module Mapper` mentioned at the begging.
- `app_path` - your application path related to `index.php` location.
- `root_paths` - your root/packages paths related to `index.php` location.
- `proxy_prefix` - proxy location from nginx configuration:

  ```
  server {
      ...
      location /auth-api {
          proxy_pass https://auth-api-nginx;
      }
      ...
  }
  ```

### 4.3 Use case

```php
<?php

declare(strict_types=1);

$moduleResolverConfig = require './../src/config/module-resolver-config.php';
$moduleMap = require './../src/config/module-map.php';

$moduleResolver = new ModuleResolver();
$moduleResolver->processRequest(
    $moduleResolverConfig,
    $moduleMap
);

$proxyPrefix = $moduleResolver->getProxyPrefix();
$processFoldersPaths = $moduleResolver->getProcessFoldersPaths();
$local = $moduleResolver->getLocal();
```


## 6. Local

## 6. Definitions Collector

1. Collect classes and packages DI definitions from : `*/config/dependencies` folder.
2. Collect classes and packages DI settings and configurations from : `*/config/settings` folder.    
   All `*.php` files are processed with an exception to settings/configuration files related to actual `$_ENV['ENV']` value.   
   Briefly. If `$_ENV['ENV'] = 'dev'` then only `dev.php` is loaded.
3. Collect translations file from : `*/config/translations` folder related to actual `$local` value.   
    Briefly. If `$local = 'en'` then only `translation_en.php` is loaded.

## 7. Module Mapper

Experimental (work in progress).
